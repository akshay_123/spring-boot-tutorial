package com.akshay.Springboot.tutorial.service;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import com.akshay.Springboot.tutorial.entity.Department;
import com.akshay.Springboot.tutorial.repository.DepartmentRepository;

@SpringBootTest
class DepartmentServiceTest {

	@Autowired
	private DepartmentService departmentService;
	
	@MockBean
	DepartmentRepository departmentRepository;

	@BeforeEach
	void setUp() throws Exception {
		Department department = Department.builder()
				.departmentName("IT").departmentId(1L)
				.departmentCode("CSE-001")
				.departmentAddress("Sangli").build();
		Mockito.when(departmentRepository.findByDepartmentNameIgnoreCase("IT"))
		.thenReturn(department);
	}

	@Test
	@DisplayName("When Department Name is Valid")
	public void whenValidDepartmentName_thenDepartmentShouldFound() {
		String departmentName = "IT";
		Department found = departmentService.findDepartmentByName(departmentName);
		assertEquals(departmentName, found.getDepartmentName());
		// fail("Not yet implemented");
	
	}

}
