package com.akshay.Springboot.tutorial.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import com.akshay.Springboot.tutorial.entity.Department;

@DataJpaTest
class DepartmentRepositoryTest {
	
	@Autowired
	private DepartmentRepository departmentRepository;
	
	@Autowired
	private TestEntityManager testEntityManager;

	@BeforeEach
	void setUp() throws Exception {
		Department department= 
				Department.builder()
				.departmentName("IT")
				.departmentCode("IT-001")
				.departmentAddress("Pune")
				.build();
		this.testEntityManager.persist(department);
	}

	@Test
	@DisplayName("When Department ID is valid")
	void whenValidDepartmentId_thenReturnDeparttment() {
		Department found= departmentRepository.findById(1L).get();
		assertEquals("IT", found.getDepartmentName());
	}

}
