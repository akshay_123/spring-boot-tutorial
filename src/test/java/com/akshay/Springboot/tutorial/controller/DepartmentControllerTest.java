package com.akshay.Springboot.tutorial.controller;

import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MockMvcBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.akshay.Springboot.tutorial.entity.Department;
import com.akshay.Springboot.tutorial.service.DepartmentService;

@WebMvcTest(DepartmentController.class)
class DepartmentControllerTest {
	
	@MockBean
	private DepartmentService departmentService;
	
	@Autowired
	private MockMvc mocMvc;
	
	private Department department;

	@BeforeEach
	void setUp() throws Exception {
		department = 
				Department.builder()
				.departmentName("IT")
				.departmentId(1L)
				.departmentCode("IT-001")
				.departmentAddress("Sangli")
				.build();
	}

	@Test
	@DisplayName("Save Department endpoint----> with valid data")
	void testAddDepartmentEndpoint() throws Exception {
		Department input= Department.builder()
				.departmentName("IT")
				.departmentCode("IT-001")
				.departmentAddress("Sangli")
				.build();
		
		Mockito.when(departmentService.addDepartment(input)).thenReturn(department);
		
		this.mocMvc.perform(MockMvcRequestBuilders.post("/departments")
				.contentType(MediaType.APPLICATION_JSON).content("{\r\n"
						+ "    \"departmentName\":\"IT\",\r\n"
						+ "    \"departmentAddress\":\"Sangli\",\r\n"
						+ "    \"departmentCode\":\"IT-001\"\r\n"
						+ "}")).andExpect(MockMvcResultMatchers.status().is(200));
	}
	
	@Test
	@DisplayName("Get Department By ID Endpoint---> with Vaild Data")
	public void testGetDepartmentByIdEndpoint() throws Exception {
		long inputId=1L;
		
		Mockito.when(this.departmentService.findDepartmentById(inputId)).thenReturn(department);
		
		this.mocMvc.perform(MockMvcRequestBuilders.get("/departments/1")
				.contentType(MediaType.APPLICATION_JSON))
		        .andExpect(MockMvcResultMatchers.status().isOk())
		        .andExpect(MockMvcResultMatchers.jsonPath("$.departmentName").
		        		value(department.getDepartmentName()));
		
	}

}
