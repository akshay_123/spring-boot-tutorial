package com.akshay.Springboot.tutorial.serviceImpl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.akshay.Springboot.tutorial.entity.Department;
import com.akshay.Springboot.tutorial.exception.DepartmentNotFoundException;
import com.akshay.Springboot.tutorial.repository.DepartmentRepository;
import com.akshay.Springboot.tutorial.service.AOPService;

@Service
public class AOPServiceImpl implements AOPService {
	
	@Autowired
	private DepartmentRepository departmentRepository;

	@Override
	public Department getDepartmentById(Long id) {
		// TODO Auto-generated method stub
		//try {
		Optional<Department> department=this.departmentRepository.findById(id);
		if(!department.isPresent()) {
			throw new DepartmentNotFoundException("Department Not found");
		}
	
		logDepartmentInDb();
		return department.get();
	}
	
	private void logDepartmentInDb() {
		System.out.println("Hi");
	}

}
