package com.akshay.Springboot.tutorial.serviceImpl;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.akshay.Springboot.tutorial.entity.Department;
import com.akshay.Springboot.tutorial.exception.DepartmentNotFoundException;
import com.akshay.Springboot.tutorial.repository.DepartmentRepository;
import com.akshay.Springboot.tutorial.service.DepartmentService;

@Service
public class DepartmentServiceImpl implements DepartmentService {
	@Autowired
	private DepartmentRepository departmentRepository;

	@Override
	public Department addDepartment(Department department) {
		// TODO Auto-generated method stub
		return departmentRepository.save(department);
	}

	@Override
	public Department findDepartmentById(Long departmentId) {
		// TODO Auto-generated method stub
		Optional<Department> departmentOpt= departmentRepository.findById(departmentId);
		if(departmentOpt.isEmpty()) {
			throw new DepartmentNotFoundException("Department is not found");
		}
		return departmentOpt.get();
	}

	@Override
	public List<Department> getAllDepartments() {
		// TODO Auto-generated method stub
		return departmentRepository.findAll();
	}

	@Override
	public void deletetDepartmentById(Long departmentId) {
		// TODO Auto-generated method stub
		departmentRepository.deleteById(departmentId);

	}

	@Override
	public Department upadteDepartment(Department department, Long departmentId) {
		// TODO Auto-generated method stub
		// Get record from DB
		Department dbDepartment = departmentRepository.findById(departmentId).get();
		
		//Set values to an entity
		if (Objects.nonNull(department.getDepartmentName()) && 
				!"".equalsIgnoreCase(department.getDepartmentName())) {
			dbDepartment.setDepartmentName(department.getDepartmentName());
		}
		if (Objects.nonNull(department.getDepartmentCode()) && 
				!"".equalsIgnoreCase(department.getDepartmentCode())) {
			dbDepartment.setDepartmentCode(department.getDepartmentCode());
		}
		if (Objects.nonNull(department.getDepartmentAddress()) && 
				!"".equalsIgnoreCase(department.getDepartmentAddress())) {
			dbDepartment.setDepartmentAddress(department.getDepartmentAddress());
		}
		
		return departmentRepository.save(dbDepartment);
	}

	@Override
	public Department findDepartmentByName(String departmentName) {
		// TODO Auto-generated method stub
		//return departmentRepository.findByDepartmentName(departmentName);
		return departmentRepository.findByDepartmentNameIgnoreCase(departmentName);
	}

	@Override
	public Department getByDepartmentCode(String departmentCode) {
		// TODO Auto-generated method stub
		return this.departmentRepository.fetchDepartmentByDepartmentCode(departmentCode);
	}
	

}
