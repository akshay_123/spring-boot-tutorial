package com.akshay.Springboot.tutorial.service;

import com.akshay.Springboot.tutorial.entity.Department;

public interface AOPService {
	Department getDepartmentById(Long id);

}
