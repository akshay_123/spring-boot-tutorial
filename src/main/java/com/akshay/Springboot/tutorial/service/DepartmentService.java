package com.akshay.Springboot.tutorial.service;

import java.util.List;

import com.akshay.Springboot.tutorial.entity.Department;

public interface DepartmentService {
	Department addDepartment(Department department);
	Department findDepartmentById(Long departmentId);
	List<Department> getAllDepartments();
	void deletetDepartmentById(Long departmentId);
	Department upadteDepartment(Department department, Long departmentId);
	Department findDepartmentByName(String departmentName);
	Department getByDepartmentCode(String departmentCode);

}
