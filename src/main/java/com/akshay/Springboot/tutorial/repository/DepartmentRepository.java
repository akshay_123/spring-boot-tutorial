package com.akshay.Springboot.tutorial.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.akshay.Springboot.tutorial.entity.Department;

@Repository
public interface DepartmentRepository extends JpaRepository<Department, Long> {
	//Using Query methods
	Department findByDepartmentName(String departmentName);
	Department findByDepartmentNameIgnoreCase(String departmentName);
	
	//Using @Query annotation
	@Query("select d from Department d where d.departmentCode = ?1")
	Department fetchDepartmentByDepartmentCode(String deparmentCode);

}
