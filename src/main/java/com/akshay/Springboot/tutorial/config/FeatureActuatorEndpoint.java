package com.akshay.Springboot.tutorial.config;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.boot.actuate.endpoint.annotation.Endpoint;
import org.springframework.boot.actuate.endpoint.annotation.ReadOperation;
import org.springframework.boot.actuate.endpoint.annotation.Selector;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Component
@Endpoint(id = "features")
public class FeatureActuatorEndpoint {
	private final Map<String, Feature> featureMap =
            new ConcurrentHashMap<>();
	
	
   public FeatureActuatorEndpoint() {
		super();
		featureMap.put("Department", new Feature(true));
		featureMap.put("Employee", new Feature(false));
		featureMap.put("Authentication", new Feature(false));
		// TODO Auto-generated constructor stub
	}
   
   @ReadOperation
   public Map<String,Feature> features(){
	   return this.featureMap;
   }
   
   @ReadOperation
   public Feature feature(@Selector String featureName){
	   return this.featureMap.get(featureName);
   }


   @Data
   @NoArgsConstructor
   @AllArgsConstructor
   public static class Feature{
	   private boolean isFeatureEnabled;
		
	}

}
