package com.akshay.Springboot.tutorial.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DemoController {
	
	@Value("${greeting.message}")
	private String message;
	@GetMapping("/")
	public String test() {
		return this.message;
	}

}
