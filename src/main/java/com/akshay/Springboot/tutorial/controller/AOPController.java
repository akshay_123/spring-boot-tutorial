package com.akshay.Springboot.tutorial.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.akshay.Springboot.tutorial.entity.Department;
import com.akshay.Springboot.tutorial.service.AOPService;

@RestController
public class AOPController {
	
	@Autowired
	private AOPService aopService;
	
	@GetMapping("/department/aop/{id}")
	public Department getDepartmentById(@PathVariable("id") Long departmentId) {
		return this.aopService.getDepartmentById(departmentId);
	}

}
