package com.akshay.Springboot.tutorial.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LoggingController {
	
	private static final Logger logger = LoggerFactory.getLogger(LoggingController.class);
	
	@GetMapping("/logging")
	public String printLog() {
		logger.info("This is sample info message");
	    logger.warn("This is sample warn message");
	    logger.error("This is sample error message");
	    logger.debug("This is sample debug message");
		return "Log has been logged";
		
	}

}
