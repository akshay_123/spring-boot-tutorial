package com.akshay.Springboot.tutorial.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.akshay.Springboot.tutorial.entity.Department;
import com.akshay.Springboot.tutorial.service.DepartmentService;

@RestController
public class DepartmentController {
	@Autowired
	private DepartmentService departmentService;
	
	private final Logger LOGGER = LoggerFactory.getLogger(DepartmentController.class) ;
	
	@PostMapping("/departments")
	public Department addDepartment(@RequestBody Department department) {
		LOGGER.info("Inside save Department() of DepartmentController ");
		return departmentService.addDepartment(department);
	}
	
	@GetMapping("departments/{id}")
	public Department getDepartmentById(@PathVariable("id") long departmentId) {
		return departmentService.findDepartmentById(departmentId);
		
	}
	
	@GetMapping("/departments")
	public List<Department> getAllDepartments(){
		return departmentService.getAllDepartments();
	}
	
	@DeleteMapping("/departments/{id}")
	public String deleteDepartmentById(@PathVariable("id") long departmentId) {
		departmentService.deletetDepartmentById(departmentId);
		return "Department deleted successfully with ID: "+departmentId;
	}
	
	@PutMapping("/departments/{id}")
	public Department updateDepartment(@RequestBody Department department, @PathVariable("id") long departmentId) {
		return departmentService.upadteDepartment(department, departmentId);
	}

	@GetMapping("/departments/name/{name}")
	public Department getDepartmentByName(@PathVariable("name") String departmentame) {
		return this.departmentService.findDepartmentByName(departmentame);
	}
	
	@GetMapping("/departments/code/{code}")
	public Department getDepartmentByCode(@PathVariable("code") String departmentCode) {
		return this.departmentService.getByDepartmentCode(departmentCode);
	}
}
