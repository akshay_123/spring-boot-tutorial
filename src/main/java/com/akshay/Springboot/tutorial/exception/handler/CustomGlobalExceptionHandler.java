package com.akshay.Springboot.tutorial.exception.handler;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.akshay.Springboot.tutorial.dto.ErrorResponse;
import com.akshay.Springboot.tutorial.exception.DepartmentNotFoundException;

@ControllerAdvice
public class CustomGlobalExceptionHandler extends ResponseEntityExceptionHandler {
	
	@ExceptionHandler(DepartmentNotFoundException.class)
	public ResponseEntity<ErrorResponse> departmentNotFound(DepartmentNotFoundException depExcep,
			WebRequest request) {
		ErrorResponse er=new ErrorResponse(HttpStatus.NOT_FOUND,depExcep.getMessage());
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(er);
		
	}
	
	

}
