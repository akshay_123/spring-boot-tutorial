package com.akshay.Springboot.tutorial.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;

/**
 * @author 10696225
 *
 */
@Aspect
@Component
@Slf4j
public class LoggingAOPClass {
	
	
	/**
	 * @Pointcut definition aasume that any method present in base packege 
	 * or within its sub packages
	 */
	@Pointcut(value="execution(* com.akshay.Springboot.tutorial.*.*.*(..))")
	public void loggingPointCut() {}
	
	/**
	 * @Pointcut definition apply to any methods inside interfaces
	 *  present in dao packege or within its sub packages
	 */
//	@Pointcut("execution(* com.xyz.someapp.dao.*.*(..))")
//	  public void dataAccessOperation() {}
	
	/**
	 * @Pointcut definition apply to any class methods within serviceImpl package
	 *   
	 */
//	@Pointcut("within(com.akshay.Springboot.tutorial.serviceImpl.*)")
//	  public void withinTheServicePackage() {}
//	
	/**
	 * @Pointcut definition apply to only single department service package
	 *   
	 */
//	@Pointcut("this(com.akshay.Springboot.tutorial.serviceImpl.DepartmentServiceImpl)")
//	  public void thisDepartmentService() {}
//	
	
	
	/**
	 * 
	 * @throws JsonProcessingException 
	 * @Before Advice
	 */
//	@Before("loggingPointCut()")
//	public void logBeforeJoinPointExecution(JoinPoint joinPoint) throws JsonProcessingException {
//		ObjectMapper om=new ObjectMapper();
//		String method=joinPoint.getSignature().getName();
//		String classname=joinPoint.getSignature().getDeclaringType().toString();
//		Object object1[]=joinPoint.getArgs();
//		Object object=null;
//		log.info("Methode invoked of class "+classname+ ":"
//		                           +method+" Arguments:"+om.writer()
//		                           .writeValueAsString(object1));
//		
//	}
	
	/**
	 * 
	 * @throws JsonProcessingException 
	 * @AfterReturning Advice
	 */
//	@AfterReturning(value = "execution(* com.akshay.Springboot.tutorial.*.*.*(..))",
//			        returning ="object" )
//	public void logAfterReturningJoinPointExecution(JoinPoint joinPoint,Object object) throws JsonProcessingException {
//		ObjectMapper om=new ObjectMapper();
//		String method=joinPoint.getSignature().getName();
//		String classname=joinPoint.getSignature().getDeclaringType().toString();
//		Object object1[]=joinPoint.getArgs();
//		log.info("Response returned in class "+classname+ ":"
//              +method+" Response:"+om.writer()
//              .writeValueAsString(object));		
//	}
	
	/**
	 * 
	 * @throws JsonProcessingException 
	 * @AfterThrowing Advice
	 */
//	@AfterThrowing(value = "execution(* com.akshay.Springboot.tutorial.*.*.*(..))",
//			        throwing ="e" )
//	public void logAfterTrowingJoinPointExecution(JoinPoint joinPoint,Exception e) throws JsonProcessingException {
//		ObjectMapper om=new ObjectMapper();
//		String method=joinPoint.getSignature().getName();
//		String classname=joinPoint.getSignature().getDeclaringType().toString();
//		Object object1[]=joinPoint.getArgs();
//		log.error("Exception generated:"+e.getMessage()+" in class "+classname+ ":"+"Method:"+method
//                +" Arguments:"+om.writer()
//                .writeValueAsString(object1)
//                );
//	}
	
	/**
	 * 
	 * @Around Advice
	 */
	@Around("loggingPointCut()")
	public Object aroundAdvice(ProceedingJoinPoint joinPoint) throws Throwable {
		ObjectMapper om=new ObjectMapper();
		String method=joinPoint.getSignature().getName();
		String classname=joinPoint.getSignature().getDeclaringType().toString();
		Object object1[]=joinPoint.getArgs();
		Object object=null;
		log.info("Methode invoked of class "+classname+ ":"
		                           +method+" Arguments:"+om.writer()
		                           .writeValueAsString(object1));
		try {
		 object=joinPoint.proceed();
		} catch (Exception e) {
			log.error("Exception generated:"+e.getMessage()+" in class "+classname+ ":"
		                           +" Arguments:"+om.writer()
		                           .writeValueAsString(object1)
		                           );
			throw e;
		}
		log.info("Response returned in class "+classname+ ":"
                +method+" Response:"+om.writer()
                .writeValueAsString(object));
		return object;
	}

}
